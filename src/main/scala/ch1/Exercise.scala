package ch1

import scala.concurrent.duration.FiniteDuration

object Exercise extends App {

  val clock: MyIO[Long] = ???

  def time[A](action: MyIO[A]): MyIO[(FiniteDuration, A)] =
    ???

  val timedHello = Exercise.time(MyIO.putStr("hello"))
    timedHello.unsafeRun() match {
      case (duration, _) => println(s"'hello' took $duration")
    } }

